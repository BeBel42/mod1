##
# Mod1
#
# @file
# @version 0.1

NAME := main.py

.PHONY: all clean fclean run

# Default rule
all:
	cython src/*.py

# Compile + run
run: all
	./$(NAME) $(file)

clean:
	$(RM) src/*.c src/*.so

# end
