import math

# from src.display import show_graph
import numpy as np

# should be multiple of 10
divide_by = 150

class Surface:
    mapSizeX = None
    mapSizeY = None

    sourcePointsFromFile = []
    sourcePointsReduced = []
    map = None
    field = None

    def add_point(self, point):
        self.sourcePointsFromFile.append(
            [
                int(point[0]),# x
                int(point[1]),# y
                int(point[2]),# z
            ]
        )

    # Define size of the map
    # Center the points on the map
    def _define_map_size(self):
        x = []
        y = []
        for point in self.sourcePointsFromFile:
            x.append(point[0])
            y.append(point[1])
        self.mapSizeX = math.floor((max(x) + min(x)) / divide_by)
        self.mapSizeY = math.floor((max(y) + min(y)) / divide_by)
        for p in self.sourcePointsFromFile:
            x = math.floor(p[0] / divide_by)
            y = math.floor(p[1] / divide_by)
            z = math.floor(p[2] / divide_by)
            self.sourcePointsReduced.append([x, y, z])
        self.sourcePointsReduced = np.array(self.sourcePointsReduced)


    def generate_all_points(self):
        self._define_map_size()
        self.map = np.full((self.mapSizeX, self.mapSizeY), None)
        self._add_input_points_to_map(self.map)
        self._set_borders()
        self._generate()
        self._check_map()

    def _add_input_points_to_map(self, map):
        for i in range(self.sourcePointsReduced.shape[0]):
            x = self.sourcePointsReduced[i][0]
            y = self.sourcePointsReduced[i][1]
            z = self.sourcePointsReduced[i][2]
            map[x][y] = z
            # self._add_x_to_compute(x)

    def _generate(self):
        # Generate the main axis of the surface every row/column where there is an existing point

        # get all unique x values
        positions_to_compute_x = np.unique(self.sourcePointsReduced[:, 0])

        # do it once for each unique x value
        for pos in positions_to_compute_x:
            self.map[pos] = self._surface_line(self.map[pos])

        # on all others axis
        for i in range(self.mapSizeY):
            self.map[:, i] = self._surface_line(self.map[:, i])

    @staticmethod
    def _surface_line(line):
        def cubic_spline_interpolation(
                Ay,
                derivateA,
                By,
                derivateB,
                num_points
        ):
            x_values = np.linspace(0, 1, num_points)

            a = Ay
            b = derivateA
            d = derivateB - 2 * By + 2 * a + b
            c = By - a - b - d



            # Generate x values between A and B

            # Compute y values using the cubic spline equation
            y_values = (
                a
                + b * (x_values)
                + c * (x_values) ** 2
                + d * (x_values) ** 3
            )

            return x_values, y_values

        points_of_interest = []
        points_of_interest_index = []
        for i in range(len(line)):
            if line[i] is not None:
                points_of_interest.append(line[i])
                points_of_interest_index.append(i)

        tangents = [None] * len(points_of_interest)
        for index, point in enumerate(points_of_interest):
            if points_of_interest[index] == 0:
                tangents[index] = 0
            else:
                tangents[index] = (
                    points_of_interest[index + 1] - points_of_interest[index - 1]
                ) / (
                    points_of_interest_index[index + 1]
                    - points_of_interest_index[index - 1]
                )

        for i in range(len(points_of_interest) - 1):

            x_a, y_a = points_of_interest_index[i], points_of_interest[i]
            dy_dx_a = tangents[i]

            x_b, y_b = points_of_interest_index[i + 1], points_of_interest[i + 1]
            dy_dx_b = tangents[i + 1]

            # Number of points between A and B
            num_points = points_of_interest_index[i + 1] - points_of_interest_index[i]

            x_values, y_values = cubic_spline_interpolation(y_a, dy_dx_a, y_b, dy_dx_b, num_points)

            for index, j in enumerate(
                range(points_of_interest_index[i], points_of_interest_index[i + 1])
            ):
                line[j] = y_values[index]

        return line

    def _set_borders(self):
        self.map[0, :] = 0
        self.map[self.mapSizeX - 1, :] = 0
        self.map[:, 0] = 0
        self.map[:, self.mapSizeY - 1] = 0

    def _check_map(self):
        # replace all None by 0 in the Map.map
        for i in range(self.mapSizeX):
            for j in range(self.mapSizeY):
                if self.map[i][j] is None:
                    print("Error, None type found in Map.map")
                    self.map[i][j] = 0

    def max(self):
        return np.max(self.map)

    def getField(self):
        if self.field is not None:
            return self.field
        m, n = self.map.shape
        r, c = np.mgrid[:m, :n]
        self.field = np.column_stack((c.ravel(), self.map.ravel(), r.ravel()))
        return self.field

    # def _add_x_to_compute(self, x):
    #     pass
        
