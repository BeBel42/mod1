import numpy as np
import open3d as o3d


class Water:

    # reference to the surface
    surface = None
    # map of the water points
    water_map = None

    # o3d objects
    water_points = None
    lineset = None

    # Changed from user input
    simulation = None

    # computed from map
    drop_size = 0.1

    # parameters
    fill_speed = 1
    rain_strength = 0.001
    wave_height = 50
    wave_side = "east"

    # statistics
    frame_count = 0

    def __init__(self, simulation, surface):
        self.surface = surface
        self.water_map = np.full((surface.mapSizeX, surface.mapSizeY), 0.0)
        self.simulation = simulation
        # water points
        self.water_points = o3d.geometry.PointCloud()
        self.lineset = o3d.geometry.LineSet()

        # estimating a good drop size
        self.drop_size = surface.max() / 200
        if self.drop_size == 0:
            self.drop_size = 0.1
        self.print_infos()


    def _add_rain(self) -> None:
        """
        Add drops to map to simulate rain.

        Applies more drops if strength is closer to 1. None if 0.
        0 < strength < 1
        """
        # Create a random mask, and add 1 where it is true
        self.water_map[np.random.rand(*self.water_map.shape) < self.rain_strength] += self.drop_size

    def _add_fill(self) -> None:
        """
        Add drops to map to simulate filling water.

        Fills faster if speed is higher. Nothing if 0.
        """
        # getting border rmask
        height, width = self.water_map.shape
        border_mask = np.zeros_like(self.water_map, dtype=bool)
        border_mask[0, :] = True  # Top row
        border_mask[height - 1, :] = True  # Bottom row
        border_mask[:, 0] = True  # Left column
        border_mask[:, width - 1] = True  # Right column

        # # getting boreder heights
        # border_heights = Map.map[border_mask] + Map.water[border_mask]
        # # lowest and highest border value
        # min_border = np.min(border_heights)
        # max_border = np.max(border_heights)
        # # how much should I add?
        # ratio = 300 * speed
        # difference = max_border - min_border
        # to_add = min(int(ratio * drop_size), (difference + drop_size) * 4)
        # # adding water to all lowest borders
        # Map.water[border_mask & (Map.water == min_border)] += to_add

        self.water_map[border_mask] += self.fill_speed * self.drop_size

    def _add_wave(self) -> None:
        """
        Add drops to map to simulate wave from a side.

        side in ('north', 'south', 'east', 'west')
        height == number of drops
        """
        # wave will not stop until height is fully filled
        wave_height = int(self.wave_height * self.drop_size)

        # arrray representing side where wave is coming from
        side_drops = None
        side_points = None

        # arrays occording to side (not match because cython does not support it)
        if self.wave_side == "north":
            side_drops = self.water_map[0, :]
            side_points = self.surface.map[0, :]
        elif self.wave_side == "south":
            side_drops = self.water_map[-1, :]
            side_points = self.surface.map[-1, :]
        elif self.wave_side == "east":
            side_drops = self.water_map[:, -1]
            side_points = self.surface.map[:, -1]
        elif self.wave_side == "west":
            side_drops = self.water_map[:, 0]
            side_points = self.surface.map[:, 0]
        else:
            raise ValueError("Invalid wave side")

        # setting drops to be height of wave
        wanted_level = wave_height - side_points
        # do not want negative drops
        wanted_level[wanted_level < 0] = 0
        # applying changes
        side_drops[:] = wanted_level

    def _get_drops_lines_points(self) -> np.ndarray:
        # all pair of points for each drop line
        drops_lines_points = np.array([]).reshape(0, 3)
        for y in range(self.water_map.shape[0]):
            for x in range(self.water_map.shape[1]):
                if self.water_map[y][x] == 0:
                    continue
                new_points = np.array(
                    [
                        [x, self.water_map[y][x] + self.surface.map[y][x], y],
                        [x, self.surface.map[y][x], y],
                    ]
                )
                drops_lines_points = np.vstack((drops_lines_points, new_points))
        return drops_lines_points

    def tic(self):
        """
        Return map status after a tick.

        Call it to apply water simulation, on each tick.
        """
        # no match because cython does not support it
        if self.simulation == "rain":
            self._add_rain()
        elif self.simulation == "fill":
            self._add_fill()
        elif self.simulation == "wave":
            self._add_wave()
        else:
            raise ValueError("Invalid simulation string")
        self._propagate_water()

        # all points for each drop lines
        drops_lines_points = self._get_drops_lines_points()
        # telling index of points for each line
        drops_lines = np.array(
            [[i * 2, i * 2 + 1] for i in (range(drops_lines_points.shape[0] // 2))]
        )
        # setting new values to geometries
        self.lineset.points = o3d.utility.Vector3dVector(drops_lines_points)
        self.lineset.lines = o3d.utility.Vector2iVector(drops_lines)
        self.water_points.points = self.lineset.points
        # applying color
        self.lineset.paint_uniform_color([0, 0, 1])
        self.water_points.paint_uniform_color([0, 0, 1])
        self.frame_count += 1

    # make the drops "flow" onto neighboring cells
    def _pass_drops_to_neighbors(self, x: int, y: int) -> None:
        """
        Move water drops to simulate water flow at point.

        x and y are the coordiantes of the point that gives its drops.
        """
        # lambda utilities
        drops = lambda xy: self.water_map[xy[1]][xy[0]]
        point = lambda xy: self.surface.map[xy[1]][xy[0]]
        height = lambda xy: drops(xy) + point(xy)

        def add_drops(xy: tuple, ndrops: float):
            self.water_map[xy[1]][xy[0]] += ndrops

        # map dimensions
        max_y, max_x = self.water_map.shape[0] - 1, self.water_map.shape[1] - 1

        left = max(0, x - 1)
        top = max(0, y - 1)
        right = min(max_x, x + 1)
        bottom = min(max_y, y + 1)
        # points that could get drops of current point
        candidates: set | list = {
            (left, top),
            (left, bottom),
            (right, top),
            (right, bottom),
            (left, y),
            (right, y),
            (x, top),
            (x, bottom),
        }

        # current point coordinates
        current = (x, y)

        # discard candidate if current point
        candidates.discard(current)

        # sorting candidates by smallest one
        candidates = list(candidates)
        candidates.sort(key=lambda x: height(x))

        # contains candidates that are not above current point
        next_candidates = candidates

        # filling all candidates
        while len(next_candidates):
            # setting current candidates to the one that are still above current
            candidates = next_candidates
            next_candidates = []

            for candidate in candidates:
                # stop if no drops left
                if drops(current) <= 0:
                    break
                hca = height(candidate)
                hcu = height(current)

                # pass water if below current
                if hca < hcu:
                    n_drops: float = max(self.drop_size, (hcu - hca) // len(candidates))
                    # passing drop to candidate
                    add_drops(current, -n_drops)
                    add_drops(candidate, n_drops)
                    # putting candidate for next iteration
                    next_candidates.append(candidate)


    # static variables to swap rows/columns to iterate on
    pattern_swap: int = 0
    start_indexes: list[tuple] = [
        (0, 0),
        (1, 1),
        (0, 1),
        (1, 0),
    ]

    # makes water drops move
    def _propagate_water(self) -> None:
        """
        Move water drops to simulate water flow.

        Will modify Map.map
        """
        # select pattern according to frame count
        start_y, start_x = self.start_indexes[self.pattern_swap]

        # map dimensions
        max_y, max_x = self.water_map.shape[0], self.water_map.shape[1]

        # iterate over map
        for y in range(start_y, max_y, 2):
            for x in range(start_x, max_x, 2):
                self._pass_drops_to_neighbors(x, y)

        # next pattern
        self.pattern_swap = (self.pattern_swap + 1) % 4

    def press_up(self):
        print("up")
        if self.simulation == "rain":
            if self.rain_strength < 0.01:
                self.rain_strength *= 2
        elif self.simulation == "fill":
            if self.fill_speed < 8:
                self.fill_speed *= 2
        elif self.simulation == "wave":
            if self.wave_height < 400:
                self.wave_height *= 2
        self.print_infos()

    def print_infos(self):
        print(self.simulation)
        if self.simulation == "rain":
            print('Rain strength', self.rain_strength)
        elif self.simulation == "fill":
            print('Fill speed', self.fill_speed)
        elif self.simulation == "wave":
            print('Wave height', self.wave_height)
        print()

    def press_down(self):
        print("down")
        if self.simulation == "rain":
            if self.rain_strength > 0.0002:
                self.rain_strength /= 2
        elif self.simulation == "fill":
            if self.fill_speed > 0.1:
                self.fill_speed /= 2
        elif self.simulation == "wave":
            if self.wave_height > 7:
                self.wave_height /= 2
        self.print_infos()
