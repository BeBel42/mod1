from time import perf_counter

import colour
import numpy as np
import open3d as o3d

from src.water import Water


class Window:

    vis = None
    surface = None
    water = None

    start_time = perf_counter()

    def __init__(self, surface):
        self._initialize_visualizer(surface)
        self.surface = surface
        self._create_surface_pcd()

    def _create_surface_pcd(self):
        # Create gradient of colours from dark brown to light yellow to white on last 10% of the map
        gradient = []
        for c in colour.Color("#663300").range_to(
                colour.Color("White"), int(np.max(self.surface.map)) + 1
        ):
            gradient.append(np.array(c.rgb))

        field_colors = []
        for i in self.surface.map.ravel():
            field_colors.append(gradient[int(i)])


        field = self.surface.getField()

        # creating mesh from point cloud
        mesh = o3d.geometry.TriangleMesh()
        mesh.vertices = o3d.utility.Vector3dVector(field)
        triangles = []
        for i in range(self.surface.mapSizeX - 1):
            row = i * self.surface.mapSizeY
            for j in range(self.surface.mapSizeY - 1):
                triangles.append([row + j, row + self.surface.mapSizeY + j + 1, row + j + 1])
                triangles.append([row + j, row + self.surface.mapSizeY + j, row + self.surface.mapSizeY + j + 1])
        mesh.vertex_colors = o3d.utility.Vector3dVector(field_colors)
        mesh.triangles = o3d.utility.Vector3iVector(triangles)
        mesh.compute_triangle_normals()
        # Effet Brillant
        mesh.compute_vertex_normals()
        # creating plane with same color as base of field
        plane = o3d.geometry.TriangleMesh.create_box(
            width=self.surface.mapSizeY, depth=self.surface.mapSizeX, height=1)
        plane.paint_uniform_color(
            [
                int("66", 16) / 255,
                int("33", 16) / 255,
                int("00", 16) / 255,
            ]
        )
        plane.translate((0, -1, 0))

        # adding static geometry to visualizer
        self.vis.add_geometry(mesh)
        self.vis.add_geometry(plane)


    def _initialize_visualizer(self, surface):
        # create o3d cisualizer and set parameters
        self.vis = o3d.visualization.VisualizerWithKeyCallback()
        self.vis.create_window()
        self.vis.get_render_option().background_color = [0, 0, 0]
        # Get the render options and increase the point size
        opt = self.vis.get_render_option()
        opt.point_size = 2


    def show(self):
        def _init_water(simulation):
            self.water = Water(simulation, self.surface)
            self.vis.add_geometry(self.water.water_points)
            self.vis.add_geometry(self.water.lineset)
            self.start_time = perf_counter()

        # display loop
        def loop(vis):
            if self.water is None:
                # sleep(0.5)
                return
            # add water and make then move if needed
            self.water.tic()
            # geometries are changed. Notify o3d to update it
            vis.update_geometry(self.water.lineset)
            vis.update_geometry(self.water.water_points)

            # poll user event. Stop if quit
            if not vis.poll_events():
                _reset_water()
                return
            # update window content
            vis.update_renderer()

        def _reset_water():
            total_time = perf_counter() - self.start_time
            print(f"Total time: {total_time:.2f} seconds")
            print(f"Frame count: {self.water.frame_count}")
            print(f"Average FPS: {self.water.frame_count / total_time:.2f}")
            self.vis.remove_geometry(self.water.water_points)
            self.vis.remove_geometry(self.water.lineset)
            self.vis.update_renderer()

        def set_rain(vis):
            _reset_water()
            _init_water("rain")

        def set_wave(vis):
            _reset_water()
            _init_water("wave")

        def set_fill(vis):
            _reset_water()
            _init_water("fill")

        def press_up(vis):
            self.water.press_up()

        def press_down(vis):
            self.water.press_down()

        _init_water("rain")

        # register weather change
        self.vis.register_key_callback(ord("1"), set_rain)  # : / key
        self.vis.register_key_callback(ord("2"), set_wave)  # . ; key
        self.vis.register_key_callback(ord("3"), set_fill)  # , < key

        # register behavior change
        self.vis.register_key_callback(85, press_up)  # up arrow
        self.vis.register_key_callback(68, press_down)  # down arrow

        self.vis.register_animation_callback(loop)
        self.vis.run()
        self.vis.destroy_window()
        # close window
        self.vis.close()
