# Mod1

Mod1 is a project that aims to simulate water flowing over a 3d generated terrain.

Mod1 is made in python, numpy and open3d. Some maps are provided in this repository.

# Preview
## Rain
Rain over some mountains
![Rain over some mountains](https://gitlab.com/BeBel42/mod1/-/raw/master/media/rain.png)
## Fill
Filling the map from all the sides
![Filling the map from all the sides](https://gitlab.com/BeBel42/mod1/-/raw/master/media/fill.png)
## Wave
A wave coming from the east
![A wave coming from the east](https://gitlab.com/BeBel42/mod1/-/raw/master/media/wave.png)

# Getting started

*Warning:* open3d does not work for python3.11 or later. You will have to use python 3.10.
You can use [Conda for python](https://towardsdatascience.com/getting-started-with-python-environments-using-conda-32e9f2779307) to use previous versions of python.

Install the dependencies with:
`pip install -r requirements.txt`

Run the program with:
`python3.10 ./main.py ./resources/demo3.mod1`

navigate in the program with:
- '1' to change to rain
- '2' to change to wave
- '3' to change to fill

- 'u' to increase the rain/wave/fill
- 'd' to decrease the rain/wave/fill

## Too slow
Please see the **Compiling the files with Cython** section.

# Compiling the files with Cython
We need performance, so we need to compile our python files to C.
Cython does exactly that. Compile the file with the makefile
`make` to compile
`make run file=<file>` to compile and run
e.g.:
`make run file=./resources/demo3.mod1`

# Development
Never execute the script manually (i.e.: python3.10 ./main.py),
because the .c files take precedence over the .py files.
You need to recompile them on each change. So use `make run`
