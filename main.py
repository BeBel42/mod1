#!/usr/bin/env python

import sys
import os

from src.surface import Surface
from src.window import Window


def parse_line(surface: Surface, line):
    index = 0
    while True:
        # skip spaces and tabs
        while (line[index] == "\t" or line[index] == " ") and index < len(line) - 1:
            index += 1
        # find start parenthesis and close parenthesis
        if index == len(line) - 1:
            return
        if line[index] != "(":
            raise Exception
        open_p = index
        index += 1
        # x coordinate
        while line[index].isnumeric():
            index += 1
        if line[index] != ",":
            raise Exception
        index += 1

        # y coordinate
        while line[index].isnumeric():
            index += 1
        if line[index] != ",":
            raise Exception
        index += 1

        # z coordinate
        while line[index].isnumeric():
            index += 1
        if line[index] != ")":
            raise Exception
        close_p = index
        index += 1

        # Add point to global map
        coordinates = [int(x) for x in line[open_p + 1: close_p].split(",")]
        surface.add_point(coordinates)


def try_open_file(surface, filename):
    if not filename.endswith(".mod1"):
        print("wrong filename: the extension should be .mod1")
        return
    if os.path.exists(filename) is False:
        print("File not found")
    else:
        f = open(filename, "r")
        lineNumber = 1
        line = f.readline()
        while line:
            try:
                parse_line(surface, line)
            except:
                print(f"Unable to parse line {lineNumber}.")
            lineNumber += 1
            line = f.readline()
        f.close()


def __main__():
    if len(sys.argv) == 2:
        surface = Surface()
        try_open_file(surface, sys.argv[1])
        surface.generate_all_points()
        window = Window(surface)
        window.show()
        # generate_surface()
    else:
        print("Usage:\n\tpython3 main.py [FILENAME.mod1]")


__main__()
